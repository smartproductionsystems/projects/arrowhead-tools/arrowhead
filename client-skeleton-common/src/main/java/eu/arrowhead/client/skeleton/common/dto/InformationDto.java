package eu.arrowhead.client.skeleton.common.dto;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Objects;

/**
 * DTO Event Informations Payload
 */
public class InformationDto implements Serializable {
    private String name;
    private String event;

    public InformationDto() {}

    public InformationDto(String name, String event) {
        this.name = name;
        this.event = event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * Objekt zu JSON-String
     *
     * @return
     */
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    /**
     * Vergleich eines Objekts mit diesem Objekt
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InformationDto that = (InformationDto) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(event, that.event);
    }

    /**
     * Pruefsumme von Objekt erzeugen
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, event);
    }
}
