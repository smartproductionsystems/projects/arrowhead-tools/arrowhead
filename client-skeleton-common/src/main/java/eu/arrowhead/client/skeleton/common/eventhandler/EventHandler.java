package eu.arrowhead.client.skeleton.common.eventhandler;

import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.common.SSLProperties;
import eu.arrowhead.common.Utilities;
import eu.arrowhead.common.dto.shared.EventPublishRequestDTO;
import eu.arrowhead.common.dto.shared.SubscriptionRequestDTO;
import eu.arrowhead.common.dto.shared.SystemRequestDTO;
import eu.arrowhead.common.dto.shared.SystemResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.*;

/**
 * Methodensammlung Event Handler
 */
@Component
public class EventHandler {
    @Autowired
    private ArrowheadService arrowheadService;

    @Autowired
    private ConfigEventProperites configEventProperites;

    @Autowired
    protected SSLProperties sslProperties;

    @Value("${arrowhead.client.subscriber.base-notification-uri:null}")
    private String baseNotificationUri;

    @Value("${arrowhead.client.system.name}")
    private String clientSystemName;

    @Value("${server.address}")
    private String clientSystemAddress;

    @Value("${server.port}")
    private int clientSystemPort;

    /**
     * Publisher abonnieren
     *
     * @param events
     */
    public void subscribe(Map<String, String> events) {
        this.subscribe(null, events);
    }

    /**
     * Publisher abonnieren
     *
     * @param providers
     */
    public void subscribe(final Set<SystemResponseDTO> providers) {
        this.subscribe(providers, configEventProperites.getEventTypeURIMap());
    }

    /**
     * Publisher abonnieren
     *
     * @param providers
     * @param events
     */
    public void subscribe(final Set<SystemResponseDTO> providers, Map<String, String> events) {
        // jedes definierte Event in application.properties abonieren
        events.forEach((event, eventUri) -> {
            SubscriptionRequestDTO subscription = this.createSubscriptionRequestDTO(event, this.getInitialSystemDto(), eventUri);

            if (providers != null && !providers.isEmpty())
                subscription.setSources(this.getSourcesFromProviders(providers));

            // bestehendes Abonnement abmelden und neu anmelden
            this.unsubscribe(event);
            arrowheadService.subscribeToEventHandler(subscription);
        });
    }

    /**
     * Abmeldung vom Publisher
     *
     * @param event
     */
    public void unsubscribe(String event) {
        arrowheadService.unsubscribeFromEventHandler(event, this.clientSystemName, this.clientSystemAddress, this.clientSystemPort);
    }

    /**
     * Nachricht veroeffentlichen
     *
     * @param event
     * @param metadata
     * @param payload
     */
    public void publish(String event, Map<String, String> metadata, String payload) {
        // Zeitstempel (UTC) setzen und Nachrichtenobjekt erzeugen
        final String timeStamp = Utilities.convertZonedDateTimeToUTCString(ZonedDateTime.now());
        final EventPublishRequestDTO request = new EventPublishRequestDTO(event, this.getInitialSystemDto(), metadata, payload, timeStamp);

        // Nachricht veroeffentlichen
        arrowheadService.publishToEventHandler(request);
    }

    private Set<SystemRequestDTO> getSourcesFromProviders(Set<SystemResponseDTO> providers) {
        Set<SystemRequestDTO> sources = new HashSet<>(providers.size());

        providers.forEach(provider -> {
            SystemRequestDTO source = new SystemRequestDTO();

            source.setSystemName(provider.getSystemName());
            source.setAddress(provider.getAddress());
            source.setPort(provider.getPort());

            sources.add(source);
        });

        return sources;
    }

    private SubscriptionRequestDTO createSubscriptionRequestDTO(final String eventType, final SystemRequestDTO subscriber, final String notificationUri) {
        return new SubscriptionRequestDTO(
                eventType.toUpperCase(),
                subscriber,
                null,
                this.baseNotificationUri + "/" + notificationUri,
                false,
                null,
                null,
                null
        );
    }

    /**
     * SystemRequestDTO mit Ausgangssysteminformationen fuellen
     * @return
     */
    private SystemRequestDTO getInitialSystemDto() {
        final SystemRequestDTO source = new SystemRequestDTO();

        source.setSystemName(this.clientSystemName);
        source.setAddress(this.clientSystemAddress);
        source.setPort(this.clientSystemPort);

        // Sicherheitsinformation setzen
        if (sslProperties.isSslEnabled())
            source.setAuthenticationInfo( Base64.getEncoder().encodeToString(arrowheadService.getMyPublicKey().getEncoded()));

        return source;
    }
}
