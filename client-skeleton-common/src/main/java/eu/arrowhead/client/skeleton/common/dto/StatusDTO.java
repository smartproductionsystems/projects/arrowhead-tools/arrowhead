package eu.arrowhead.client.skeleton.common.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO fuer Statusinformation zur Matrix
 */
public class StatusDTO implements Serializable {
    private boolean isSuccess;
    private boolean isMatrixOn;
    private String message;

    public StatusDTO() {}

    public StatusDTO(boolean isSuccess, boolean isMatrixOn, String message) {
        this.isSuccess = isSuccess;
        this.isMatrixOn = isMatrixOn;
        this.message = message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public boolean isMatrixOn() {
        return isMatrixOn;
    }

    public void setMatrixOn(boolean matrixOn) {
        isMatrixOn = matrixOn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * erzeugt String aus Objekt
     *
     * @return
     */
    @Override
    public String toString() {
        return "StatusDTO{" +
                "isSuccess=" + isSuccess +
                ", isMatrixOn=" + isMatrixOn +
                ", message='" + message + '\'' +
                '}';
    }

    /**
     * Vergleich eines Objekts mit diesem Objekt
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatusDTO statusDTO = (StatusDTO) o;
        return isSuccess() == statusDTO.isSuccess() &&
                isMatrixOn() == statusDTO.isMatrixOn() &&
                Objects.equals(getMessage(), statusDTO.getMessage());
    }

    /**
     * Pruefsumme von Objekt erzeugen
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(isSuccess(), isMatrixOn(), getMessage());
    }
}
