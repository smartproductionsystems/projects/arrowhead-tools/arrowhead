package eu.arrowhead.client.skeleton.common.dto;

import java.awt.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * DTO fuer Matrix Informationen
 */
public class MatrixDTO implements Serializable {
    private boolean turnOn;
    private Color[][] ledColor;

    //-------------------------------------------------------------------------------------------------
    public MatrixDTO() {}

    //-------------------------------------------------------------------------------------------------
    public MatrixDTO(boolean turnOn) {
        this.turnOn = turnOn;
    }

    //-------------------------------------------------------------------------------------------------
    public MatrixDTO(boolean turnOn, Color[][] ledColor) {
        this.turnOn = turnOn;
        this.ledColor = ledColor;
    }

    //-------------------------------------------------------------------------------------------------
    public boolean isTurnOn() {
        return turnOn;
    }

    //-------------------------------------------------------------------------------------------------
    public void setTurnOn(boolean turnOn) {
        this.turnOn = turnOn;
    }

    //-------------------------------------------------------------------------------------------------
    public Color[][] getLedColor() {
        return ledColor;
    }

    //-------------------------------------------------------------------------------------------------
    public void setLedColor(Color[][] ledColor) {
        this.ledColor = ledColor;
    }

    //-------------------------------------------------------------------------------------------------
    /**
     * erzeugt String aus Objekt
     *
     * @return
     */
    @Override
    public String toString() {
        return "MatrixDTO{" +
                "turnOn=" + turnOn +
                ", ledColor=" + Arrays.toString(ledColor) +
                '}';
    }

    //-------------------------------------------------------------------------------------------------
    /**
     * Vergleich eines Objekts mit diesem Objekt
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatrixDTO matrixDTO = (MatrixDTO) o;
        return turnOn == matrixDTO.turnOn &&
                Arrays.equals(ledColor, matrixDTO.ledColor);
    }

    //-------------------------------------------------------------------------------------------------
    /**
     * Pruefsumme von Objekt erzeugen
     *
     * @return
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(turnOn);
        result = 31 * result + Arrays.hashCode(ledColor);
        return result;
    }
}
