package eu.arrowhead.client.skeleton.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * DTO fuer Sensor Informationen
 */
public class SensorDTO implements Serializable {
    private Timestamp timestamp;
    private double data;
    private String errorMessage;

    public SensorDTO() {}

    public SensorDTO(Timestamp timestamp, double data) {
        this.timestamp = timestamp;
        this.data = data;
        this.errorMessage = null;
    }

    public SensorDTO(Timestamp timestamp, String errorMessage) {
        this.timestamp = timestamp;
        this.errorMessage = errorMessage;
        this.data = Double.MIN_VALUE;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public double getData() {
        return data;
    }

    public void setData(double data) {
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * erzeugt String aus Objekt
     *
     * @return
     */
    @Override
    public String toString() {
        return "SensorDTO{" +
                "timestamp=" + timestamp +
                ", data=" + data +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }

    /**
     * Vergleich eines Objekts mit diesem Objekt
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorDTO sensorDTO = (SensorDTO) o;
        return Double.compare(sensorDTO.getData(), getData()) == 0 &&
                Objects.equals(getTimestamp(), sensorDTO.getTimestamp()) &&
                Objects.equals(getErrorMessage(), sensorDTO.getErrorMessage());
    }

    /**
     * Pruefsumme von Objekt erzeugen
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(getTimestamp(), getData(), getErrorMessage());
    }
}
