package eu.arrowhead.client.skeleton.common.serviceregistry;

import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.common.dto.shared.ServiceRegistryRequestDTO;
import eu.arrowhead.common.dto.shared.ServiceSecurityType;
import eu.arrowhead.common.dto.shared.SystemRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Methodensammlung Service Registry
 */
@Component
public class ServiceRegistry {
    @Autowired
    private ArrowheadService arrowheadService;

    @Value("${arrowhead.client.system.name}")
    private String clientSystemName;
    @Value("${server.address}")
    private String clientSystemAddress;
    @Value("${server.port}")
    private int clientSystemPort;

    @Value("${arrowhead.client.system.interface.secure}")
    private String interfaceSecure;
    @Value("${arrowhead.client.system.interface.insecure}")
    private String interfaceInsecure;

    @Value("${server.ssl.enabled}")
    private boolean sslEnabled;
    @Value("${token.security.filter.enabled:false}")
    private boolean tokenSecurityFilterEnabled;

    /**
     * Anmeldung eines Diensts
     *
     * @param serviceDefinition
     * @param serviceUri
     */
    public void register(String serviceDefinition, String serviceUri) {
        this.register(serviceDefinition, serviceUri, null, null);
    }

    /**
     * Anmeldung eines Diensts
     *
     * @param serviceDefinition
     * @param serviceUri
     * @param httpMethod
     */
    public void register(String serviceDefinition, String serviceUri, HttpMethod httpMethod) {
        this.register(serviceDefinition, serviceUri, httpMethod, null);
    }

    /**
     * Anmeldung eines Diensts
     *
     * @param serviceDefinition
     * @param serviceUri
     * @param httpMethod
     * @param metadata
     */
    public void register(String serviceDefinition, String serviceUri, HttpMethod httpMethod, Map<String, String> metadata) {
        final ServiceRegistryRequestDTO serviceRegistryRequest = new ServiceRegistryRequestDTO();
        serviceRegistryRequest.setServiceDefinition(serviceDefinition);
        final SystemRequestDTO systemRequest = new SystemRequestDTO();

        // Setzen der eigenen Systembeschreibung
        systemRequest.setSystemName(this.clientSystemName);
        systemRequest.setAddress(this.clientSystemAddress);
        systemRequest.setPort(this.clientSystemPort);

        // Sicherheitsinformationen und Schnittstellen setzen setzen
        if (tokenSecurityFilterEnabled) {
            systemRequest.setAuthenticationInfo(Base64.getEncoder().encodeToString(arrowheadService.getMyPublicKey().getEncoded()));
            serviceRegistryRequest.setSecure(ServiceSecurityType.TOKEN);
            serviceRegistryRequest.setInterfaces(List.of(this.interfaceSecure));
        } else if (sslEnabled) {
            systemRequest.setAuthenticationInfo(Base64.getEncoder().encodeToString(arrowheadService.getMyPublicKey().getEncoded()));
            serviceRegistryRequest.setSecure(ServiceSecurityType.CERTIFICATE);
            serviceRegistryRequest.setInterfaces(List.of(this.interfaceSecure));
        } else {
            serviceRegistryRequest.setSecure(ServiceSecurityType.NOT_SECURE);
            serviceRegistryRequest.setInterfaces(List.of(this.interfaceInsecure));
        }

        serviceRegistryRequest.setProviderSystem(systemRequest);
        serviceRegistryRequest.setServiceUri(serviceUri);
        serviceRegistryRequest.setMetadata(new HashMap<>());

        // Metadaten-Beschreibung setzen
        if (httpMethod != null)
            serviceRegistryRequest.getMetadata().put("http-method", httpMethod.name());

        if (metadata != null && !metadata.isEmpty())
            serviceRegistryRequest.getMetadata().putAll(metadata);

        // Registrieren des Diensts
        arrowheadService.forceRegisterServiceToServiceRegistry(serviceRegistryRequest);
    }

    /**
     * Abmeldung des Diensts
     *
     * @param serviceDefinition
     */
    public void unregister(String serviceDefinition) {
        arrowheadService.unregisterServiceFromServiceRegistry(serviceDefinition);
    }
}
