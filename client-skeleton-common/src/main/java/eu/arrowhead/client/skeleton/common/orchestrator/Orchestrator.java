package eu.arrowhead.client.skeleton.common.orchestrator;

import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.common.SSLProperties;
import eu.arrowhead.common.dto.shared.*;
import eu.arrowhead.common.dto.shared.OrchestrationFlags.Flag;
import eu.arrowhead.common.dto.shared.OrchestrationFormRequestDTO.Builder;
import eu.arrowhead.common.exception.ArrowheadException;
import eu.arrowhead.common.exception.InvalidParameterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Methodensammlung Orchestrator
 */
@Component
public class Orchestrator {
    @Autowired
    private ArrowheadService arrowheadService;

    @Autowired
    protected SSLProperties sslProperties;

    @Value("${arrowhead.client.system.interface.secure}")
    private String interfaceSecure;
    @Value("${arrowhead.client.system.interface.insecure}")
    private String interfaceInsecure;
    @Resource(name = "arrowheadContext")
    private Map<String, Object> arrowheadContext;

    @Value("${arrowhead.client.orchestration.max-retry:0}")
    private int orchestrationMaxRetry;
    @Value("${arrowhead.client.orchestration.retry-wait:1}")
    private int orchestrationRetryWait;

    @Value("${arrowhead.client.orchestration.matchmaking:false}")
    private boolean orchestrationOptionMatchmaking;
    @Value("${arrowhead.client.orchestration.override-store:true}")
    private boolean orchestrationOptionOverrideStore;
    @Value("${arrowhead.client.orchestration.metadata-search:true}")
    private boolean orchestrationOptionMetadataSearch;
    @Value("${arrowhead.client.orchestration.enable-inter-cloud:false}")
    private boolean orchestrationOptionEnableInterCloud;
    @Value("${arrowhead.client.orchestration.enable-qos:false}")
    private boolean orchestrationOptionEnableQos;
    @Value("${arrowhead.client.orchestration.external-service-request:false}")
    private boolean orchestrationOptionExternalServiceRequest;
    @Value("${arrowhead.client.orchestration.only-preferred:false}")
    private boolean orchestrationOptionOnlyPreferred;
    @Value("${arrowhead.client.orchestration.trigger-inter-cloud:false}")
    private boolean orchestrationOptionTriggerInterCloud;
    @Value("${arrowhead.client.orchestration.ping-providers:true}")
    private boolean orchestrationOptionPingProviders;

    /**
     * Orchestrierungsanfrage
     *
     * @param serviceDefinition
     * @param serviceRequirement
     * @param httpMethod
     * @return
     * @throws ArrowheadException
     * @throws NullPointerException
     * @throws InterruptedException
     */
    public List<OrchestrationResultDTO> orchestrate(final String serviceDefinition, final String serviceRequirement, HttpMethod httpMethod) throws ArrowheadException, NullPointerException, InterruptedException {
        return this.orchestrate(serviceDefinition, serviceRequirement, httpMethod, new HashMap<String, String>());
    }

    /**
     * Orchestrierungsanfrage
     *
     * @param serviceDefinition
     * @param serviceRequirement
     * @param httpMethod
     * @param metadata
     * @return
     * @throws ArrowheadException
     * @throws InterruptedException
     */
    public List<OrchestrationResultDTO> orchestrate(final String serviceDefinition, final String serviceRequirement, HttpMethod httpMethod, Map<String, String> metadata) throws ArrowheadException, InterruptedException {
        int retryCounter = 0;
        OrchestrationResponseDTO orchestrationResponse = null;

        // Dienstinformation erstellen
        ServiceQueryFormDTO serviceQueryForm = new ServiceQueryFormDTO.Builder(serviceDefinition)
                .interfaces(serviceRequirement)                        // Schnittstelle
                .pingProviders(true)                                    // Provider pingen
                .version(0, 100)                                        // Dienstversion setzen
                .metadata("http-method", httpMethod.name())             // Metadaten fuer HTTP-Methode
                .metadata(metadata)                                     // andere Metadaten setzen
                .security(ServiceSecurityType.NOT_SECURE)               // keine Sicherheitsmassnahmen benoetigt
                .build();
        Builder orchestrationFormBuilder = arrowheadService.getOrchestrationFormBuilder();

        // Orchestrierungseigenschaften einstellen
        OrchestrationFormRequestDTO orchestrationFormRequest = orchestrationFormBuilder
                .requestedService(serviceQueryForm)
                .flag(Flag.MATCHMAKING, this.orchestrationOptionMatchmaking)                            // nur Dienst mit exaktem Namen zurueckgeben
                .flag(Flag.OVERRIDE_STORE, this.orchestrationOptionOverrideStore)                       // Orchestrator Store ueberschreiben
                .flag(Flag.METADATA_SEARCH, this.orchestrationOptionMetadataSearch)                     // Suchen mit Metadaten
                .flag(Flag.ENABLE_INTER_CLOUD, this.orchestrationOptionEnableInterCloud)                // Pruefung Verwendung andere Clouds
                .flag(Flag.ENABLE_QOS, this.orchestrationOptionEnableQos)                               // QoS verwenden
                .flag(Flag.EXTERNAL_SERVICE_REQUEST, this.orchestrationOptionExternalServiceRequest)
                .flag(Flag.ONLY_PREFERRED, this.orchestrationOptionOnlyPreferred)                       // nur bevorzugte Provider pruefen
                .flag(Flag.TRIGGER_INTER_CLOUD, this.orchestrationOptionTriggerInterCloud)              // andere Clouds anfragen
                .flag(Flag.PING_PROVIDERS, this.orchestrationOptionPingProviders)                       // Provider pingen
                .build();

        // Orchestrierung durchfuehren
        do {
            orchestrationResponse = arrowheadService.proceedOrchestration(orchestrationFormRequest);

            // Bei Erfolg Schleife beenden
            // Bei Fehler kurz warten und noch einmal versuchen
            if (orchestrationResponse.getResponse().isEmpty() && this.orchestrationMaxRetry > 0)
                TimeUnit.SECONDS.sleep(this.orchestrationRetryWait);
            else
                break;
        } while((retryCounter++) < this.orchestrationMaxRetry);

        // Fehlerwurf bei leerem Ergebnis
        if (orchestrationResponse.getResponse().isEmpty())
            throw new ArrowheadException("Orchestrations Response is empty");

        // Rueckgabe des Ergebnisses
        return orchestrationResponse.getResponse();
    }

    /**
     * Validierung des Orchestrierungsergebnisses
     *
     * @param orchestrationResult
     * @param serviceDefinition
     * @return
     */
    public boolean validateOrchestrationResult(final OrchestrationResultDTO orchestrationResult, final String serviceDefinition) {
        boolean hasValidInterface = false;

        if (!orchestrationResult.getService().getServiceDefinition().equalsIgnoreCase(serviceDefinition))
            throw new InvalidParameterException("Requested and orchestrated service definition do not match");

        for (final ServiceInterfaceResponseDTO serviceInterface : orchestrationResult.getInterfaces()) {
            if (serviceInterface.getInterfaceName().equalsIgnoreCase(getInterface())) {
                hasValidInterface = true;
                break;
            }
        }

        if (!hasValidInterface)
            throw new InvalidParameterException("Requested and orchestrated interface do not match");
        else
            return true;
    }

    /**
     * Rueckgabe der zu verwendeten Schnittstelle
     * Entscheidung auf Basis des Eigenschaft "SSL enabled"
     *
     * @return
     */
    private String getInterface() {
        return sslProperties.isSslEnabled() ? interfaceSecure : interfaceInsecure;
    }

    //-------------------------------------------------------------------------------------------------
    public void setOrchestrationOptionMatchmaking(boolean orchestrationOptionMatchmaking) {
        this.orchestrationOptionMatchmaking = orchestrationOptionMatchmaking;
    }

    public void setOrchestrationOptionOverrideStore(boolean orchestrationOptionOverrideStore) {
        this.orchestrationOptionOverrideStore = orchestrationOptionOverrideStore;
    }

    public void setOrchestrationOptionMetadataSearch(boolean orchestrationOptionMetadataSearch) {
        this.orchestrationOptionMetadataSearch = orchestrationOptionMetadataSearch;
    }

    public void setOrchestrationOptionEnableInterCloud(boolean orchestrationOptionEnableInterCloud) {
        this.orchestrationOptionEnableInterCloud = orchestrationOptionEnableInterCloud;
    }

    public void setOrchestrationOptionEnableQos(boolean orchestrationOptionEnableQos) {
        this.orchestrationOptionEnableQos = orchestrationOptionEnableQos;
    }

    public void setOrchestrationOptionExternalServiceRequest(boolean orchestrationOptionExternalServiceRequest) {
        this.orchestrationOptionExternalServiceRequest = orchestrationOptionExternalServiceRequest;
    }

    public void setOrchestrationOptionOnlyPreferred(boolean orchestrationOptionOnlyPreferred) {
        this.orchestrationOptionOnlyPreferred = orchestrationOptionOnlyPreferred;
    }

    public void setOrchestrationOptionTriggerInterCloud(boolean orchestrationOptionTriggerInterCloud) {
        this.orchestrationOptionTriggerInterCloud = orchestrationOptionTriggerInterCloud;
    }

    public void setOrchestrationOptionPingProviders(boolean orchestrationOptionPingProviders) {
        this.orchestrationOptionPingProviders = orchestrationOptionPingProviders;
    }
}
