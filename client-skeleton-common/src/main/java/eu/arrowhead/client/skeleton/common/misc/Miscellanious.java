package eu.arrowhead.client.skeleton.common.misc;

import eu.arrowhead.common.Utilities;

public class Miscellanious {
    public static void printOut(Object object) {
        System.out.println(Utilities.toPrettyJson(Utilities.toJson(object)));
    }
}
