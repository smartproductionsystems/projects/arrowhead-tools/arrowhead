package eu.arrowhead.client.skeleton.common;

/**
 * Konstanten fuer das komplette Projekt
 */
public class Constants {
    // PUBLISHER
    //-----------------------------------------------------------------------------------------------------
    public static final String EVENT_TYPE_START_INIT = "START_INIT";
    public static final String EVENT_TYPE_START_RUN = "START_RUN";
    public static final String EVENT_TYPE_REQUEST_RECEIVED = "REQUEST_RECEIVED";
    public static final String EVENT_TYPE_REQUEST_RECEIVED_METADATA_REQUEST_TYPE = "REQUEST_TYPE";
    public static final String EVENT_TYPE_FACE_DETECTED = "FACE_DETECTED";
    public static final String EVENT_TYPE_PUBLISHER_DESTROYED = "PUBLISHER_DESTROYED";

    public static final String START_INIT_EVENT_PAYLOAD = "InitStarted";
    public static final String START_RUN_EVENT_PAYLOAD = "RunStarted";
    public static final String FACE_DETECTED_PAYLOAD = "FaceDetected";
    public static final String PUBLISHER_DESTROYED_EVENT_PAYLOAD= "DestroyStarted";

    // SUBSCRIBER
    //-----------------------------------------------------------------------------------------------------
    public static final String SUBSCRIBER_NOTIFICATION_QUEUE_NAME = "notifications";
    public static final String SUBSCRIBER_EVENT_TASK_NAME = "eventtask";
    public static final String SUBSCRIBER_DEFAULT_EVENT_URI = "/notify";
}
