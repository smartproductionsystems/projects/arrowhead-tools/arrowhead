package eu.arrowhead.client.skeleton.provider.controller;

import de.larsgrefer.sense_hat.SenseHat;
import de.larsgrefer.sense_hat.SenseHatColor;
import eu.arrowhead.client.skeleton.common.dto.SensorDTO;
import eu.arrowhead.client.skeleton.common.dto.MatrixDTO;
import eu.arrowhead.client.skeleton.common.dto.StatusDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import eu.arrowhead.common.CommonConstants;
import java.awt.*;
import java.io.IOException;
import java.sql.Timestamp;

/**
 * Controller fuer REST Kommunikation ueber HTTP/HTTPS-Schnittstelle
 */
@RestController
public class ProviderController {
	private boolean isOn = false;

	/**
	 * Endpoint fuer Test
	 *
	 * @return
	 */
	@GetMapping(path = CommonConstants.ECHO_URI)
	public ResponseEntity echoService() {
		return new ResponseEntity("Got it!", HttpStatus.OK);
	}

	/**
	 * Ausgabe des Luftdrucks mit aktuellen Timestamp
	 *
	 * @return
	 */
	@GetMapping(path = "/api/pressure")
	public ResponseEntity getPressure() {
		HttpStatus hs = HttpStatus.OK;
		SensorDTO s = new SensorDTO();

		try {
			SenseHat sh = new SenseHat();

			s = new SensorDTO(new Timestamp(System.currentTimeMillis()), sh.getPressure());
			hs = HttpStatus.OK;
		} catch (IOException e) {
			s = new SensorDTO(new Timestamp(System.currentTimeMillis()), e.getMessage());
			hs = HttpStatus.BAD_REQUEST;
		} finally {
			return new ResponseEntity<>(s, hs);
		}
	}

	/**
	 * Ausgabe der Temperatur mit aktuellen Timestamp
	 *
	 * @return
	 */
	@GetMapping(path = "/api/temperature")
	public ResponseEntity getTemp() throws IOException {
		HttpStatus hs = HttpStatus.OK;
		SensorDTO s = new SensorDTO();

		try {
			SenseHat sh = new SenseHat();

			s = new SensorDTO(new Timestamp(System.currentTimeMillis()), ((sh.getTemperature() + sh.getTemperatureFromHumidity() + sh.getTemperatureFromPressure()) / 3.0));
			hs = HttpStatus.OK;
		} catch (IOException e) {
			s = new SensorDTO(new Timestamp(System.currentTimeMillis()), e.getMessage());
			hs = HttpStatus.BAD_REQUEST;
		} finally {
			return new ResponseEntity<>(s, hs);
		}
	}

	/**
	 * Ausgabe der Luftfeuchte mit aktuellen Timestamp
	 *
	 * @return
	 */
	@GetMapping(path = "/api/humidity")
	public ResponseEntity getHumidity() {
		HttpStatus hs = HttpStatus.OK;
		SensorDTO s = new SensorDTO();

		try {
			SenseHat sh = new SenseHat();

			s = new SensorDTO(new Timestamp(System.currentTimeMillis()), sh.getHumidity());
			hs = HttpStatus.OK;
		} catch (IOException e) {
			s = new SensorDTO(new Timestamp(System.currentTimeMillis()), e.getMessage());
			hs = HttpStatus.BAD_REQUEST;
		} finally {
			return new ResponseEntity<>(s, hs);
		}
	}

	/**
	 * Ausgabe des Matrixzustands
	 *
	 * @return
	 */
	@GetMapping(path = "/api/matrix/power")
	public ResponseEntity getMatrixPower() {
		StatusDTO s = new StatusDTO(true, this.isOn, null);

		return new ResponseEntity<>(s, HttpStatus.OK);
	}

	/**
	 * Aendern des Matrixzustands an/aus
	 *
	 * @return
	 */
	@PostMapping(path = "/api/matrix/power")
	public ResponseEntity switchPower(@RequestBody MatrixDTO matrixDTO) {
		SenseHat sh;
		SenseHatColor c;
		HttpStatus hs = HttpStatus.OK;
		StatusDTO s = new StatusDTO(false, this.isOn, "Not initialized");

		try {
			sh = new SenseHat();

			if (matrixDTO.isTurnOn()) {
				this.isOn = true;
				c = SenseHatColor.WHITE;
			} else {
				this.isOn = false;
				c = SenseHatColor.BLACK;
			}

			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++)
					sh.setPixel(i, j, c);

			s = new StatusDTO(true, this.isOn, null);
		} catch (IOException e) {
			hs = HttpStatus.BAD_REQUEST;
			s = new StatusDTO(false, this.isOn, e.getMessage());
		} finally {
			return new ResponseEntity<>(s, HttpStatus.OK);
		}
	}

	/**
	 * Aendern der Matrixfarbe
	 *
	 * @return
	 */
	@PostMapping(path = "/api/matrix/light")
	public ResponseEntity setLedLights(@RequestBody MatrixDTO matrixDTO) {
		SenseHatColor[][] shc = this.translateFromColor(matrixDTO.getLedColor());
		SenseHat sh;
		HttpStatus hs = HttpStatus.OK;
		StatusDTO s = new StatusDTO(false, this.isOn, "Not initialized");

		try {
			sh = new SenseHat();
			sh.setColors(shc);

			this.isOn = true;
			hs = HttpStatus.OK;
			s = new StatusDTO(true, this.isOn, null);
		} catch (Exception e) {
			hs = HttpStatus.BAD_REQUEST;
			s = new StatusDTO(false, this.isOn, e.getMessage());
		} finally {
			return new ResponseEntity<>(s, hs);
		}
	}

	//-------------------------------------------------------------------------------------------------
	/**
	 * Uebersetzung Farbmatrix mit Color zu SenseHatColor
	 *
	 * @param colorMatrix
	 * @return
	 */
	private SenseHatColor[][] translateFromColor(Color[][] colorMatrix) {
		SenseHatColor[][] shc = new SenseHatColor[8][8];

		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++)
				shc[i][j] = SenseHatColor.fromColor(colorMatrix[i][j]);

		return shc;
	}
}
