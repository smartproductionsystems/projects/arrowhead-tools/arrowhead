package eu.arrowhead.client.skeleton.provider;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.larsgrefer.sense_hat.SenseHat;
import de.larsgrefer.sense_hat.SenseHatColor;
import eu.arrowhead.client.skeleton.common.serviceregistry.ServiceRegistry;
import eu.arrowhead.common.dto.shared.ServiceRegistryRequestDTO;
import eu.arrowhead.common.dto.shared.ServiceRegistryResponseDTO;
import eu.arrowhead.common.dto.shared.ServiceSecurityType;
import eu.arrowhead.common.dto.shared.SystemRequestDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.client.library.config.ApplicationInitListener;
import eu.arrowhead.client.library.util.ClientCommonConstants;
import eu.arrowhead.client.skeleton.provider.security.ProviderSecurityConfig;
import eu.arrowhead.common.CommonConstants;
import eu.arrowhead.common.Utilities;
import eu.arrowhead.common.core.CoreSystem;
import eu.arrowhead.common.exception.ArrowheadException;

@Component
public class ProviderApplicationInitListener extends ApplicationInitListener {
	private SenseHat ht;
	
	@Autowired
	private ArrowheadService arrowheadService;
	
	@Autowired
	private ProviderSecurityConfig providerSecurityConfig;

	@Autowired
	private ServiceRegistry serviceRegistry;
	
	@Value(ClientCommonConstants.$TOKEN_SECURITY_FILTER_ENABLED_WD)
	private boolean tokenSecurityFilterEnabled;
	
	@Value(CommonConstants.$SERVER_SSL_ENABLED_WD)
	private boolean sslEnabled;
	
	private final Logger logger = LogManager.getLogger(ProviderApplicationInitListener.class);

	//-------------------------------------------------------------------------------------------------
	@Override
	protected void customInit(final ContextRefreshedEvent event) {
		Map<String, String> metadata = new HashMap<>();

		//Checking the availability of necessary core systems
		checkCoreSystemReachability(CoreSystem.SERVICE_REGISTRY);

		if (sslEnabled && tokenSecurityFilterEnabled) {
			checkCoreSystemReachability(CoreSystem.AUTHORIZATION);			

			//Initialize Arrowhead Context
			arrowheadService.updateCoreServiceURIs(CoreSystem.AUTHORIZATION);			
		
			setTokenSecurityFilter();
		} else {
			logger.info("TokenSecurityFilter in not active");
		}

		metadata.put("data-hardware", "RPi SenseHat");

		// Registrierung der Dienste
		metadata.put("data-type", "Temperature");
		serviceRegistry.register("get-information-temperature",  "/api/temperature", HttpMethod.GET, metadata);

		metadata.put("data-type", "Humidity");
		serviceRegistry.register("get-information-humidity",  "/api/humidity", HttpMethod.GET, metadata);

		metadata.put("data-type", "Pressure");
		serviceRegistry.register("get-information-pressure",  "/api/pressure", HttpMethod.GET, metadata);

		metadata.put("data-type", "Information");
		serviceRegistry.register("get-information-matrix-power",  "/api/matrix/power", HttpMethod.GET, metadata);

		metadata.put("data-type", "State");
		serviceRegistry.register("set-information-matrix-power",  "/api/matrix/power", HttpMethod.POST, metadata);

		metadata.put("data-type", "Light");
		serviceRegistry.register("set-information-matrix-light",  "/api/matrix/light", HttpMethod.POST, metadata);

		try {
			ht = new SenseHat();
			ht.setPixel(0,0, SenseHatColor.GREEN);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//-------------------------------------------------------------------------------------------------
	@Override
	public void customDestroy() {
		// Abmeldung der Dienste und Farbmatrix verdunkeln
		serviceRegistry.unregister("get-information-temperature");
		serviceRegistry.unregister("get-information-pressure");
		serviceRegistry.unregister("get-information-humidity");
		serviceRegistry.unregister("get-information-matrix-power");
		serviceRegistry.unregister("set-information-matrix-power");
		serviceRegistry.unregister("set-information-matrix-light");

		try {
			ht.setPixel(0,0,SenseHatColor.BLACK);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------------------------------------
	private void setTokenSecurityFilter() {
		final PublicKey authorizationPublicKey = arrowheadService.queryAuthorizationPublicKey();

		if (authorizationPublicKey == null)
			throw new ArrowheadException("Authorization public key is null");
		
		KeyStore keystore;
		try {
			keystore = KeyStore.getInstance(sslProperties.getKeyStoreType());
			keystore.load(sslProperties.getKeyStore().getInputStream(), sslProperties.getKeyStorePassword().toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
			throw new ArrowheadException(ex.getMessage());
		}

		final PrivateKey providerPrivateKey = Utilities.getPrivateKey(keystore, sslProperties.getKeyPassword());
		
		providerSecurityConfig.getTokenSecurityFilter().setAuthorizationPublicKey(authorizationPublicKey);
		providerSecurityConfig.getTokenSecurityFilter().setMyPrivateKey(providerPrivateKey);
	}
}
