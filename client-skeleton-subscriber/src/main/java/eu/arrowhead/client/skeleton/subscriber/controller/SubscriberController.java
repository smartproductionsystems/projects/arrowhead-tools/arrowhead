package eu.arrowhead.client.skeleton.subscriber.controller;

import com.google.gson.Gson;
import com.pi4j.wiringpi.Gpio;
import eu.arrowhead.client.skeleton.common.Constants;
import eu.arrowhead.client.skeleton.common.dto.InformationDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import eu.arrowhead.common.CommonConstants;
import eu.arrowhead.common.dto.shared.EventDTO;
import javax.annotation.Resource;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Controller fuer REST Kommunikation ueber HTTP/HTTPS-Schnittstelle
 */
@RestController
@RequestMapping(path = Constants.SUBSCRIBER_DEFAULT_EVENT_URI)
public class SubscriberController {
	// Warteschlange fuer Events im Projekt
	@Resource(name = Constants.SUBSCRIBER_NOTIFICATION_QUEUE_NAME)
	private ConcurrentLinkedQueue<EventDTO> notificatonQueue;

	@Value("${arrowhead.client.subscriber.ledpanel-path:/home/pi/i2cdisplay/ledpanel.py}")
	private String ledPanelPath;

	private final int LCD_COLUMNS = 16;
	private final Logger logger = LogManager.getLogger(SubscriberController.class);

	/**
	 * Initialisierung WiringPi
	 */
	static {
		Gpio.wiringPiSetup();
	}

	//-------------------------------------------------------------------------------------------------
	/**
	 * Kann zum Testen verwendet werden, ob Subscriber erfolgreich gestartet ist (GET Anfrage)
	 *
	 * @return
	 */
	@GetMapping(path = CommonConstants.ECHO_URI)
	public ResponseEntity echoService() {
		return new ResponseEntity("Got it!", HttpStatus.OK);
	}
	
	//-------------------------------------------------------------------------------------------------
	/**
	 * Benachrichtigung, wenn Publisher Anfrage erhalten hat (POST Anfrage)
	 *
	 * @param event
	 * @return
	 */
	@PostMapping(path = "/requestreceived")
	public ResponseEntity receivePublisherReceivedRequestEvent(@RequestBody final EventDTO event ) {
		logger.debug("receivePublisherReceivedRequestEvent started...");
		
		if( event.getEventType() == null)
			logger.debug("EventType is null.");

		// Event Warteschalnge fuellen
		notificatonQueue.add(event);

		return new ResponseEntity(HttpStatus.ACCEPTED);
	}
	
	//-------------------------------------------------------------------------------------------------
	/**
	 * Benachrichtigung, wenn Publisher initialisiert worden ist (POST Anfrage)
	 *
	 * @param event
	 * @return
	 */
	@PostMapping(path = "/startinit")
	public ResponseEntity receivePublisherStartedInitEvent(@RequestBody final EventDTO event) {
		logger.debug("receivePublsisherStartedInitEvent started... ");
		
		if( event.getEventType() == null)
			logger.debug("EventType is null.");

		// Event Warteschalnge fuellen
		notificatonQueue.add(event);

		return new ResponseEntity(HttpStatus.ACCEPTED);
	}
	
	//-------------------------------------------------------------------------------------------------
	/**
	 * Benachrichtigung, wenn Publisher gestartet ist (POST Anfrage)
	 *
	 * @param event
	 * @return
	 */
	@PostMapping(path = "/startrun")
	public ResponseEntity receivePublisherStartedRunEvent(@RequestBody final EventDTO event ) {
		logger.debug("receivePublsisherStartedRunEvent started... ");
		
		if( event.getEventType() == null)
			logger.debug("EventType is null.");

		// Event Warteschalnge fuellen
		notificatonQueue.add(event);

		return new ResponseEntity(HttpStatus.ACCEPTED);
	}

	//-------------------------------------------------------------------------------------------------

	/**
	 * Benachrichtigung, wenn Publisher Gesicht erkannt hat (POST Anfrage)
	 *
	 * @param event
	 * @return
	 * @throws Exception
	 */
	@PostMapping(path = "/facedetected")
	public ResponseEntity receivePublisherFaceDetectedEvent(@RequestBody final EventDTO event) throws Exception {
		if (event.getPayload().isEmpty() || event.getPayload().isBlank())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);

		// Fuellen der Event Warteschlange
		notificatonQueue.add(event);

		// Parsen des Payloads von String in InformationDTO und Ausgabe auf LCD-Display
		InformationDto informationDto = new Gson().fromJson(event.getPayload(), InformationDto.class);
		String[] command = {"/usr/bin/python3", this.ledPanelPath, informationDto.getName().substring(0, ((informationDto.getName().length() <= this.LCD_COLUMNS) ? informationDto.getName().length() : this.LCD_COLUMNS)) };

		Runtime.getRuntime().exec(command);

		return new ResponseEntity(HttpStatus.ACCEPTED);
	}
}
