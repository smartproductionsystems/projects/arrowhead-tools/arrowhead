package eu.arrowhead.client.skeleton.subscriber;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import eu.arrowhead.client.skeleton.common.eventhandler.ConfigEventProperites;
import eu.arrowhead.client.skeleton.common.Constants;
import eu.arrowhead.client.skeleton.common.eventhandler.EventHandler;
import eu.arrowhead.client.skeleton.subscriber.misc.EventNotifier;
import eu.arrowhead.common.dto.shared.EventDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.client.library.config.ApplicationInitListener;
import eu.arrowhead.client.library.util.ClientCommonConstants;
import eu.arrowhead.client.skeleton.subscriber.security.SubscriberSecurityConfig;
import eu.arrowhead.common.CommonConstants;
import eu.arrowhead.common.Utilities;
import eu.arrowhead.common.core.CoreSystem;
import eu.arrowhead.common.exception.ArrowheadException;
import javax.annotation.Resource;

/**
 * Initialisierungsklasse
 * Ausfuehrung unmittelbar nach und vor dem vollendetem Start
 */
@Component
public class SubscriberApplicationInitListener extends ApplicationInitListener {
	@Autowired
	private ArrowheadService arrowheadService;
	
	@Autowired
	private SubscriberSecurityConfig subscriberSecurityConfig;

	@Autowired
	private EventHandler eventHandler;

	@Autowired
	private ConfigEventProperites configEventProperites;

	@Autowired
	private ApplicationContext applicationContext;
	
	@Value(ClientCommonConstants.$TOKEN_SECURITY_FILTER_ENABLED_WD)
	private boolean tokenSecurityFilterEnabled;
	
	@Value(CommonConstants.$SERVER_SSL_ENABLED_WD)
	private boolean sslEnabled;
	
	@Value(ClientCommonConstants.$CLIENT_SYSTEM_NAME)
	private String clientSystemName;
	
	@Value(ClientCommonConstants.$CLIENT_SERVER_ADDRESS_WD)
	private String clientSystemAddress;
	
	@Value(ClientCommonConstants.$CLIENT_SERVER_PORT_WD)
	private int clientSystemPort;
	
	private final Logger logger = LogManager.getLogger(SubscriberApplicationInitListener.class);

	@Resource(name = Constants.SUBSCRIBER_NOTIFICATION_QUEUE_NAME)
	private ConcurrentLinkedQueue<EventDTO> notificatonQueue;

	//-------------------------------------------------------------------------------------------------
	@Override
	protected void customInit(final ContextRefreshedEvent event) {
		//Checking the availability of necessary core systems
		checkCoreSystemReachability(CoreSystem.SERVICE_REGISTRY);
		checkCoreSystemReachability(CoreSystem.ORCHESTRATOR);
		checkCoreSystemReachability(CoreSystem.EVENT_HANDLER);
		
		if (sslEnabled && tokenSecurityFilterEnabled) {
			checkCoreSystemReachability(CoreSystem.AUTHORIZATION);

			//Initialize Arrowhead Context
			arrowheadService.updateCoreServiceURIs(CoreSystem.AUTHORIZATION);		
			
			setTokenSecurityFilter();
			setNotificationFilter();
		} else {
			logger.info("TokenSecurityFilter in not active");
		}

		arrowheadService.updateCoreServiceURIs(CoreSystem.ORCHESTRATOR);
		arrowheadService.updateCoreServiceURIs(CoreSystem.SERVICE_REGISTRY);
		arrowheadService.updateCoreServiceURIs(CoreSystem.EVENT_HANDLER);

		// asynchrone Event Benachrichtigung initialisieren
		final EventNotifier eventNotifier = applicationContext.getBean(Constants.SUBSCRIBER_EVENT_TASK_NAME, EventNotifier.class);
		eventNotifier.start();
	}


	//-------------------------------------------------------------------------------------------------
	/**
	 * Abmelden von allen abonnierten Events
	 */
	@Override
	public void customDestroy() {
		final Map<String, String> eventTypeMap = configEventProperites.getEventTypeURIMap();

		if (eventTypeMap != null)
			for (final String eventType : eventTypeMap.keySet())
				arrowheadService.unsubscribeFromEventHandler(eventType, clientSystemName, clientSystemAddress, clientSystemPort);
	}

	//-------------------------------------------------------------------------------------------------
	private void setTokenSecurityFilter() {
		final PublicKey authorizationPublicKey = arrowheadService.queryAuthorizationPublicKey();
		final Map<String, String> eventTypeMap = configEventProperites.getEventTypeURIMap();
		KeyStore keystore;

		if (authorizationPublicKey == null)
			throw new ArrowheadException("Authorization public key is null");

		try {
			keystore = KeyStore.getInstance(sslProperties.getKeyStoreType());
			keystore.load(sslProperties.getKeyStore().getInputStream(), sslProperties.getKeyStorePassword().toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
			throw new ArrowheadException(ex.getMessage());
		}

		final PrivateKey subscriberPrivateKey = Utilities.getPrivateKey(keystore, sslProperties.getKeyPassword());
		
		subscriberSecurityConfig.getTokenSecurityFilter().setEventTypeMap(eventTypeMap);
		subscriberSecurityConfig.getTokenSecurityFilter().setAuthorizationPublicKey(authorizationPublicKey);
		subscriberSecurityConfig.getTokenSecurityFilter().setMyPrivateKey(subscriberPrivateKey);
	}
	
	//-------------------------------------------------------------------------------------------------
	private void setNotificationFilter() {
		logger.debug("setNotificationFilter started...");
		
		final Map<String, String> eventTypeMap = configEventProperites.getEventTypeURIMap();

		subscriberSecurityConfig.getNotificationFilter().setEventTypeMap(eventTypeMap);
		subscriberSecurityConfig.getNotificationFilter().setServerCN(arrowheadService.getServerCN());		
	}
}
