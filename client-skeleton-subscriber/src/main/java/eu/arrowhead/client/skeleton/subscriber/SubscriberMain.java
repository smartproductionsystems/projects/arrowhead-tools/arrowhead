package eu.arrowhead.client.skeleton.subscriber;

import eu.arrowhead.client.skeleton.common.Constants;
import eu.arrowhead.client.skeleton.common.eventhandler.ConfigEventProperites;
import eu.arrowhead.client.skeleton.subscriber.misc.EventNotifier;
import eu.arrowhead.common.dto.shared.EventDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import eu.arrowhead.common.CommonConstants;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Starterklasse
 */
@SpringBootApplication
@EnableConfigurationProperties(ConfigEventProperites.class)
@ComponentScan(basePackages = {CommonConstants.BASE_PACKAGE})
public class SubscriberMain {
	public static void main(final String[] args) {
		SpringApplication.run(SubscriberMain.class, args);
	}

	/**
	 * Initialisierung Event Warteschlange fuer ankommende Events
	 *
	 * @return
	 */
	@Bean(name = Constants.SUBSCRIBER_NOTIFICATION_QUEUE_NAME)
	public ConcurrentLinkedQueue<EventDTO> getNotificationQueue() {
		return new ConcurrentLinkedQueue<>();
	}

	/**
	 * Initialisierung Task Thread
	 *
	 * @return
	 */
	@Bean(name = Constants.SUBSCRIBER_EVENT_TASK_NAME)
	public EventNotifier getEventTask() {
		return new EventNotifier();
	}
}
