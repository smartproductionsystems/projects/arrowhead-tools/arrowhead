package eu.arrowhead.client.skeleton.subscriber.misc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.annotation.Resource;
import eu.arrowhead.client.skeleton.common.Constants;
import eu.arrowhead.client.skeleton.common.eventhandler.EventHandler;
import eu.arrowhead.client.skeleton.common.orchestrator.Orchestrator;
import eu.arrowhead.common.dto.shared.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;

/**
 * asynchrone Ausgabe von abonnierten Events
 */
public class EventNotifier extends Thread {
    private boolean interrupted = false;
    private final Logger logger = LogManager.getLogger(EventNotifier.class);

    @Resource(name = Constants.SUBSCRIBER_NOTIFICATION_QUEUE_NAME)
    private ConcurrentLinkedQueue<EventDTO> notificatonQueue;

    @Autowired
    private Orchestrator orchestrator;

    @Autowired
    private EventHandler eventHandler;

    //-------------------------------------------------------------------------------------------------

    /**
     * kontinuierliche Ausgabe eingehender abonnierter Events
     */
    @Override
    public void run() {
        final Set<SystemResponseDTO> sources = new HashSet<>();
        List<OrchestrationResultDTO> faceRecognitionProvider = null;

        interrupted = Thread.currentThread().isInterrupted();

        // Publisher Dienst holen
        try {
            faceRecognitionProvider = orchestrator.orchestrate("publish-face-recognition-data", "HTTP-INSECURE-JSON", HttpMethod.PUT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Beenden des Thread, falls kein Dienst gefunden werden konnte
        if (faceRecognitionProvider.isEmpty())
            Thread.currentThread().interrupt();

        // Dienst abonnieren
        sources.add(faceRecognitionProvider.get(0).getProvider());
        eventHandler.subscribe(sources);

        // Ausgabe der Eventbenachrichtigungen, so lange nicht beendet wird
        while (!interrupted) {
            try {
                if (notificatonQueue.peek() != null) {
                    for (final EventDTO event : notificatonQueue)
                        System.out.println("Event: " + event.getEventType() + ", Payload: " + event.getPayload());

                    notificatonQueue.clear();
                }
            } catch (final Throwable ex) {
                logger.info(ex.getMessage());
            }
        }
    }

    //-------------------------------------------------------------------------------------------------
    /**
     * sauberes Beenden des Diensts
     */
    public void destroy() {
        logger.debug("ConsumerTask.destroy started...");
        interrupted = true;
    }
}
