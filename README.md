# Bitte Umzug beachten - Please note relocation
[https://iversion.informatik.htw-dresden.de/smartproductionsystems/projects/arrowhead-tools/DemonstratorInfineon](https://iversion.informatik.htw-dresden.de/smartproductionsystems/projects/arrowhead-tools/DemonstratorInfineon)

---

# Infineon Arrowhead Demonstrator - Client
Der Client-Demonstrator soll die grundlegende Funktionsweise des Arrowhead Frameworks zeigen.

## vorgefertigte Images
Vorgefertigte Images können im Branch `images` gefunden werden. Diese müssen nur noch auf eine SD-Karte geflasht und eine Anpassung der dnsmasq.conf vorgenommen werden.

## 2. Verwendung
1.  `git clone https://gitlab.com/smartproductionsystems/projects/arrowhead-tools/arrowhead.git`
2.  Anpassung der application.properties
3.  `mvn clean install`
4.  Kopieren der JAR-Packages auf ihre Systeme (<MODUL>/target/arrowhead-client-skeleton-*.jar)
5.  Ausführen der Java-Datei auf dem Systeme (Reihenfolge beachten: Producer vor Consumer, Publisher vor Subscriber)\
    `java -jar arrowhead-client-skeleton-*.jar`

## 3. Version
Der Client wurde mit der Arrowhead-Version 4.1.3.5 entwickelt.

## 4. Fördergeber
![Sachsen](assets/sponsors/Saxony.gif "Sachsen")
![BMBF](assets/sponsors/BMBF.jpg "BMBF")\
Diese Maßnahme wird mitfinanziert mit Steuermitteln auf Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

![Ecsel JU](assets/sponsors/Ecsel_JU.jpg "Ecsel JU")
![Europa](assets/sponsors/Europe.png "Europa")\
This project has received funding from the ECSEL Joint Undertaking (JU) under grant agreement No 826452. The JU receives support from the European Union’s Horizon 2020 research and innovation programme and Sweden, Austria, Spain, Poland, Germany, Italy, Czech Republic, Netherlands, Belgium, Latvia, Romania, France, Hungary, Portugal, Finland, Turkey, Norway, Switzerland.

