package eu.arrowhead.client.skeleton.publisher;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import eu.arrowhead.client.skeleton.common.Constants;
import eu.arrowhead.client.skeleton.common.eventhandler.EventHandler;
import eu.arrowhead.client.skeleton.common.serviceregistry.ServiceRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.client.library.config.ApplicationInitListener;
import eu.arrowhead.client.library.util.ClientCommonConstants;
import eu.arrowhead.client.skeleton.publisher.event.PresetEventType;
import eu.arrowhead.client.skeleton.publisher.security.PublisherSecurityConfig;
import eu.arrowhead.common.CommonConstants;
import eu.arrowhead.common.Utilities;
import eu.arrowhead.common.core.CoreSystem;
import eu.arrowhead.common.exception.ArrowheadException;

/**
 * Initialisierungsklasse
 * Ausfuehrung unmittelbar nach und vor dem vollendetem Start
 */
@Component
public class PublisherApplicationInitListener extends ApplicationInitListener {
	@Autowired
	private ArrowheadService arrowheadService;
	
	@Autowired
	private PublisherSecurityConfig publisherSecurityConfig;

	@Autowired
	private ServiceRegistry serviceRegistry;

	@Autowired
	private EventHandler eventHandler;
	
	@Value(ClientCommonConstants.$TOKEN_SECURITY_FILTER_ENABLED_WD)
	private boolean tokenSecurityFilterEnabled;
	
	@Value(CommonConstants.$SERVER_SSL_ENABLED_WD)
	private boolean sslEnabled;
	
	private final Logger logger = LogManager.getLogger(PublisherApplicationInitListener.class);

	//-------------------------------------------------------------------------------------------------
	@Override
	protected void customInit(final ContextRefreshedEvent event) {
		//Checking the availability of necessary core systems
		checkCoreSystemReachability(CoreSystem.SERVICE_REGISTRY);
		checkCoreSystemReachability(CoreSystem.EVENT_HANDLER);
		
		if (sslEnabled && tokenSecurityFilterEnabled) {
			checkCoreSystemReachability(CoreSystem.AUTHORIZATION);			

			//Initialize Arrowhead Context
			arrowheadService.updateCoreServiceURIs(CoreSystem.AUTHORIZATION);			
		
			setTokenSecurityFilter();
		} else {
			logger.info("TokenSecurityFilter in not active");
		}

		// Gesichtserkennungsdienst registrieren, wenn Event Handler aktiv vorhanden ist
		if (arrowheadService.echoCoreSystem(CoreSystem.EVENT_HANDLER)) {
			arrowheadService.updateCoreServiceURIs(CoreSystem.SERVICE_REGISTRY);
			arrowheadService.updateCoreServiceURIs(CoreSystem.EVENT_HANDLER);

			serviceRegistry.register("publish-face-recognition-data", "/api/facerecognition", HttpMethod.PUT);
		}
	}

	//-------------------------------------------------------------------------------------------------
	@Override
	public void customDestroy() {
		// Beendigungsnachricht veröffentlichen und Dienst abmelden
		eventHandler.publish(PresetEventType.PUBLISHER_DESTROYED.getEventTypeName(), null, Constants.PUBLISHER_DESTROYED_EVENT_PAYLOAD);
		serviceRegistry.unregister("publish-face-recognition-data");
	}

	//-------------------------------------------------------------------------------------------------
	private void setTokenSecurityFilter() {
		KeyStore keystore;
		PublicKey authorizationPublicKey = arrowheadService.queryAuthorizationPublicKey();
		PrivateKey publisherPrivateKey;

		if (authorizationPublicKey == null)
			throw new ArrowheadException("Authorization public key is null");

		try {
			keystore = KeyStore.getInstance(sslProperties.getKeyStoreType());
			keystore.load(sslProperties.getKeyStore().getInputStream(), sslProperties.getKeyStorePassword().toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
			throw new ArrowheadException(ex.getMessage());
		}

		publisherPrivateKey = Utilities.getPrivateKey(keystore, sslProperties.getKeyPassword());
		
		publisherSecurityConfig.getTokenSecurityFilter().setAuthorizationPublicKey(authorizationPublicKey);
		publisherSecurityConfig.getTokenSecurityFilter().setMyPrivateKey(publisherPrivateKey);
	}
}
