package eu.arrowhead.client.skeleton.publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import eu.arrowhead.common.CommonConstants;

/**
 * Starterklasse
 */
@SpringBootApplication
@ComponentScan(basePackages = {CommonConstants.BASE_PACKAGE})
public class PublisherMain {
	public static void main(final String[] args) {
		SpringApplication.run(PublisherMain.class, args);
	}
}
