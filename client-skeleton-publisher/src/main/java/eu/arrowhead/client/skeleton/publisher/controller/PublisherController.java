package eu.arrowhead.client.skeleton.publisher.controller;

import java.util.Map;

import eu.arrowhead.client.skeleton.common.Constants;
import eu.arrowhead.client.skeleton.common.dto.InformationDto;
import eu.arrowhead.client.skeleton.common.eventhandler.EventHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import eu.arrowhead.client.skeleton.publisher.event.PresetEventType;
import eu.arrowhead.common.CommonConstants;
import static eu.arrowhead.client.skeleton.common.Constants.FACE_DETECTED_PAYLOAD;

/**
 * Controller fuer REST Kommunikation ueber HTTP/HTTPS-Schnittstelle
 */
@RestController
public class PublisherController {
	private final Logger logger = LogManager.getLogger(PublisherController.class);

	@Autowired
	private EventHandler eventHandler;

	//-------------------------------------------------------------------------------------------------

	/**
	 * Event "REQUEST_RECEIVED" an Abonennten publizieren
	 *
	 * @return
	 */
	@GetMapping(path = CommonConstants.ECHO_URI)
	public ResponseEntity echoService() {
		logger.debug("echoService started...");

		// Event publizieren
		eventHandler.publish(PresetEventType.REQUEST_RECEIVED.getEventTypeName(), Map.of(Constants.EVENT_TYPE_REQUEST_RECEIVED_METADATA_REQUEST_TYPE, HttpMethod.GET.name()), CommonConstants.ECHO_URI);

		return new ResponseEntity("Got it!", HttpStatus.OK);
	}

	//-------------------------------------------------------------------------------------------------
	/**
	 * Event "FACE_DETECTED" mit Payload Name an Abonennten publizieren, falls ein Gesicht bzw. niemand erkannt wurde
	 *
	 * @param informationDto
	 * @return
	 */
	@PutMapping(path = "/api/facerecognition")
	public ResponseEntity receiveName(@RequestBody InformationDto informationDto) {
		logger.debug("request received: " + informationDto.toString());

		if (informationDto.getName().isEmpty() || informationDto.getName() == "")
			return new ResponseEntity(HttpStatus.BAD_REQUEST);

		informationDto.setEvent(FACE_DETECTED_PAYLOAD);

		// Nachricht mit Name an Abonnenten publizieren
		eventHandler.publish(PresetEventType.FACE_DETECTED.getEventTypeName(), Map.of(Constants.EVENT_TYPE_FACE_DETECTED, HttpMethod.PUT.name()), informationDto.toString());

		return new ResponseEntity(HttpStatus.OK);
	}
}
