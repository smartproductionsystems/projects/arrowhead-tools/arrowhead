package eu.arrowhead.client.skeleton.consumer;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import eu.arrowhead.client.skeleton.common.dto.MatrixDTO;
import eu.arrowhead.client.skeleton.common.dto.StatusDTO;
import eu.arrowhead.client.skeleton.common.orchestrator.Orchestrator;
import eu.arrowhead.client.skeleton.consumer.misc.LedSignal;
import eu.arrowhead.client.skeleton.consumer.misc.MatrixColorMapper;
import eu.arrowhead.common.dto.shared.*;
import eu.arrowhead.common.exception.ArrowheadException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.client.library.config.ApplicationInitListener;
import eu.arrowhead.common.core.CoreSystem;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Initialisierungsklasse
 * Ausfuehrung unmittelbar nach und vor dem vollendetem Start
 */
@Component
public class ConsumerApplicationInitListener extends ApplicationInitListener {
	@Autowired
	private ArrowheadService arrowheadService;

	@Autowired
	private Orchestrator orchestrator;
	
	private final Logger logger = LogManager.getLogger(ConsumerApplicationInitListener.class);

	@Value("${arrowhead.gpio.led:GPIO_25}")
	private String gpioPinLed;
	@Value("${arrowhead.gpio.button.power:GPIO_01}")
	private String gpioPinMatrixPower;
	@Value("${arrowhead.gpio.button.light.red:GPIO_00}")
	private String gpioPinMatrixRed;
	@Value("${arrowhead.gpio.button.light.blue:GPIO_02}")
	private String gpioPinMatrixBlue;
	@Value("${arrowhead.gpio.button.light.green:GPIO_03}")
	private String gpioPinMatrixGreen;

	@Override
	protected void customInit(final ContextRefreshedEvent event) {
		List<OrchestrationResultDTO> resultDTO = null;
		Map<String, String> metadata = new HashMap<>();

		metadata.put("data-hardware", "RPi SenseHat");

		// Pruefung Core-Systeme vorhanden; wirft Fehler bei nicht vorhanden sein
		checkCoreSystemReachability(CoreSystem.SERVICE_REGISTRY);
		checkCoreSystemReachability(CoreSystem.ORCHESTRATOR);

		// Initialisiert Arrowhead Kontext
		arrowheadService.updateCoreServiceURIs(CoreSystem.SERVICE_REGISTRY);
		arrowheadService.updateCoreServiceURIs(CoreSystem.ORCHESTRATOR);

		try {
			// Dienstorchestrierung fuer Matrix-Licht-Steuerung anfragen
			resultDTO = orchestrator.orchestrate("set-information-matrix-light", "HTTP-INSECURE-JSON", HttpMethod.POST, metadata);

			// Events fuer die Buttons erzeugen
			buttonListener(RaspiPin.getPinByName(this.gpioPinMatrixRed), resultDTO.get(0), true, Color.RED);			//RaspiPin.GPIO_00
			buttonListener(RaspiPin.getPinByName(this.gpioPinMatrixBlue), resultDTO.get(0), true, Color.BLUE);			//RaspiPin.GPIO_02
			buttonListener(RaspiPin.getPinByName(this.gpioPinMatrixGreen), resultDTO.get(0), true, Color.GREEN);			//RaspiPin.GPIO_03

			// Dienstorchestrierung fuer Matrix-Power-Steuerung anfragen
			resultDTO = orchestrator.orchestrate("set-information-matrix-power", "HTTP-INSECURE-JSON", HttpMethod.POST, metadata);

			buttonListener(RaspiPin.getPinByName(this.gpioPinMatrixPower), resultDTO.get(0), true, null);			//RaspiPin.GPIO_01

			// LED Status-Anzeige, dass Consumer bereit ist
			LedSignal l = new LedSignal(RaspiPin.getPinByName(this.gpioPinLed), 500);		//RaspiPin.GPIO_25
			l.run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	//-------------------------------------------------------------------------------------------------
	/**
	 * Ausfuehrung vor dem ordentlichen Beenden des Programms
	 */
	@Override
	public void customDestroy() {
		//TODO: implement here any custom behavior on application shutdown
	}

	//-------------------------------------------------------------------------------------------------
	/**
	 * Event Listener - Hoert auf das Druecken eines GPIO-Buttons
	 * @param pin
	 * @param result
	 * @param turnOn
	 * @param color
	 */
	private void buttonListener(final Pin pin, final OrchestrationResultDTO result, final boolean turnOn, final Color color) {
		// Erstellung GPIO Instanz und Button-Eigenschaft
		final GpioController gpio = GpioFactory.getInstance();
		final GpioPinDigitalInput button = gpio.provisionDigitalInputPin(pin, PinPullResistance.PULL_DOWN);

		// Erstellung Event zum Button Druecken
		button.setShutdownOptions(true);
		button.addListener(new GpioPinListenerDigital() {
			private boolean matrixTurnOn = turnOn;

			/**
			 * Ausfuehrung beim Statuswechsel (gedrueckt, losgelassen) des Buttons
			 *
			 * @param gpioPinDigitalStateChangeEvent
			 */
			@Override
			public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent gpioPinDigitalStateChangeEvent) {
				if (button.getState() == PinState.HIGH)
					return;

				try {
					Color[][] colorMatrix = MatrixColorMapper.singleColor(color);
					String colorName = "null";

					if (color != null)
						colorName = color.toString();

					// Kommunikation mit Dienst zur Steuerung der Matrix
					MatrixDTO matrix = new MatrixDTO(this.matrixTurnOn, colorMatrix);
					StatusDTO statusDTO = arrowheadService.consumeServiceHTTP(
							StatusDTO.class,
							HttpMethod.valueOf(result.getMetadata().get("http-method")),
							result.getProvider().getAddress(),
							result.getProvider().getPort(),
							result.getServiceUri(),
							result.getInterfaces().get(0).getInterfaceName(),
							null,
							matrix
					);

					if (color == null)
						this.matrixTurnOn = !this.matrixTurnOn;

					System.out.println("Button pressed: Pin=" + pin.getName() + ", turnOn=" + this.matrixTurnOn + ", Color=" + colorName);
				} catch (ArrowheadException e) {
					System.err.println("Button pressed: Pin=" + pin.getName() + ", error=" + e.getMessage());
				}
			}
		});
	}
}
