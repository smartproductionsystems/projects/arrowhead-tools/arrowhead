package eu.arrowhead.client.skeleton.consumer.misc;

import java.awt.*;

/**
 * Farbenzuordnung der LED-Matrix
 */
public class MatrixColorMapper {
    /**
     * Fuellt eine Color-Matrix mit einheitlicher Farbe
     * @param color
     * @return
     */
    public static Color[][] singleColor(Color color) {
        // Rueckgabe null, falls keine Farbe uebergeben wurde
        if (color == null)
            return null;

        // Farbmatrix erzeugen
        Color[][] c = new Color[8][8];

        // Farbmatrix mit einheitlicher Farbe fuellen
        for (int i = 0; i < c.length; i++)
            for (int j = 0; j < c[0].length; j++)
                c[i][j] = color;

        // Farbmatrix zurueckgeben
        return c;
    }
}
