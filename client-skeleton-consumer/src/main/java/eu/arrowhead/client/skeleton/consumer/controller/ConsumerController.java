package eu.arrowhead.client.skeleton.consumer.controller;

import eu.arrowhead.client.library.ArrowheadService;
import eu.arrowhead.client.skeleton.common.dto.MatrixDTO;
import eu.arrowhead.client.skeleton.common.dto.SensorDTO;
import eu.arrowhead.client.skeleton.common.dto.StatusDTO;
import eu.arrowhead.client.skeleton.consumer.misc.MatrixColorMapper;
import eu.arrowhead.client.skeleton.common.orchestrator.Orchestrator;
import eu.arrowhead.common.dto.shared.OrchestrationResultDTO;
import eu.arrowhead.common.exception.ArrowheadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller fuer REST Kommunikation ueber HTTP/HTTPS-Schnittstelle
 */
@RestController
public class ConsumerController {
    // Speicherung Service-Verbindung zu Service-Beschreibung
    private Map<String, OrchestrationResultDTO> data = new HashMap<>();

    @Autowired
    private Orchestrator orchestrator;

    @Autowired
    private ArrowheadService arrowheadService;

    /**
     * Holen der Verbindungsinformation zu Service Beschreibung und Speicherung des Arrowhead Response
     * in Map; Wird nicht ausgefuehrt, wenn Funktion bereits erfolgreich ausgefuehrt wurde
     *
     * @param serviceDefinition
     * @param serviceRequirements
     * @param httpMethod
     * @return
     */
    private boolean receiveOrchestrationData(String serviceDefinition, String serviceRequirements, HttpMethod httpMethod) {
        if (data.containsKey(serviceDefinition))
            return true;

        try {
            List<OrchestrationResultDTO> resultDto = orchestrator.orchestrate(serviceDefinition, serviceRequirements, httpMethod);

            if (resultDto.isEmpty() || resultDto == null)
                return false;

            this.data.put(serviceDefinition, resultDto.get(0));
        } catch (ArrowheadException | NullPointerException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Temperaturdaten von Provider holen und ausgeben (GET Anfrage)
     *
     * @return
     */
    @GetMapping(path = "/temperature")
    public ResponseEntity getTemperature() {
        // Fehlerausgabe, falls Dienst nicht gefunden werden kann
        if (!receiveOrchestrationData("get-information-temperature", "HTTP-INSECURE-JSON", HttpMethod.GET))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Temperaturdaten von Dienst holen
        OrchestrationResultDTO result = this.data.get("get-information-temperature");
        SensorDTO sensorDTO = arrowheadService.consumeServiceHTTP(
                SensorDTO.class,
                HttpMethod.valueOf(result.getMetadata().get("http-method")),
                result.getProvider().getAddress(),
                result.getProvider().getPort(),
                result.getServiceUri(),
                result.getInterfaces().get(0).getInterfaceName(),
                null,
                null
        );

        // Ausgabe
        if (sensorDTO.getErrorMessage() == null)
            return new ResponseEntity<>(sensorDTO, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Luftdruckdaten von Provider holen und ausgeben (GET Anfrage)
     *
     * @return
     */
    @GetMapping(path = "/pressure")
    public ResponseEntity getPressure() {
        // Fehlerausgabe, falls Dienst nicht gefunden werden kann
        if (!receiveOrchestrationData("get-information-pressure", "HTTP-INSECURE-JSON", HttpMethod.GET))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Temperaturdaten von Dienst holen
        OrchestrationResultDTO result = this.data.get("get-information-pressure");
        SensorDTO sensorDTO = arrowheadService.consumeServiceHTTP(
                SensorDTO.class,
                HttpMethod.valueOf(result.getMetadata().get("http-method")),
                result.getProvider().getAddress(),
                result.getProvider().getPort(),
                result.getServiceUri(),
                result.getInterfaces().get(0).getInterfaceName(),
                null,
                null
        );

        // Ausgabe
        if (sensorDTO.getErrorMessage() == null)
            return new ResponseEntity<>(sensorDTO, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Luftfeuchtedaten von Provider holen und ausgeben (GET Anfrage)
     *
     * @return
     */
    @GetMapping(path = "/humidity")
    public ResponseEntity getHumidity() {
        // Fehlerausgabe, falls Dienst nicht gefunden werden kann
        if (!receiveOrchestrationData("get-information-humidity", "HTTP-INSECURE-JSON", HttpMethod.GET))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Temperaturdaten von Dienst holen
        OrchestrationResultDTO result = this.data.get("get-information-humidity");
        SensorDTO sensorDTO = arrowheadService.consumeServiceHTTP(
                SensorDTO.class,
                HttpMethod.valueOf(result.getMetadata().get("http-method")),
                result.getProvider().getAddress(),
                result.getProvider().getPort(),
                result.getServiceUri(),
                result.getInterfaces().get(0).getInterfaceName(),
                null,
                null
        );

        // Ausgabe
        if (sensorDTO.getErrorMessage() == null)
            return new ResponseEntity<>(sensorDTO, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Matrixstatus von Provider holen und ausgeben (GET Anfrage)
     *
     * @return
     */
    @GetMapping(path = "/power")
    public ResponseEntity getPowerStatus() {
        // Fehlerausgabe, falls Dienst nicht gefunden werden kann
        if (!receiveOrchestrationData("get-information-matrix-power", "HTTP-INSECURE-JSON", HttpMethod.GET))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Matrixstatus von Dienst holen
        OrchestrationResultDTO result = this.data.get("get-information-matrix-power");
        StatusDTO statusDTO = arrowheadService.consumeServiceHTTP(
                StatusDTO.class,
                HttpMethod.valueOf(result.getMetadata().get("http-method")),
                result.getProvider().getAddress(),
                result.getProvider().getPort(),
                result.getServiceUri(),
                result.getInterfaces().get(0).getInterfaceName(),
                null,
                null
        );

        // Ausgabe
        if (statusDTO.getMessage() == null)
            return new ResponseEntity<>(statusDTO, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Matrix beim Provider an-/ausschalten (POST Anfrage)
     *
     * @return
     */
    @PostMapping(path = "/power")
    public ResponseEntity setPowerStatus(@RequestBody MatrixDTO matrixDTO) {
        // Fehlerausgabe, falls Dienst nicht gefunden werden kann
        if (!receiveOrchestrationData("set-information-matrix-power", "HTTP-INSECURE-JSON", HttpMethod.POST))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Matrixstatus beim Provider setzen
        MatrixDTO matrix = new MatrixDTO(matrixDTO.isTurnOn(), null);
        OrchestrationResultDTO result = this.data.get("set-information-matrix-power");
        StatusDTO statusDTO = arrowheadService.consumeServiceHTTP(
                StatusDTO.class,
                HttpMethod.valueOf(result.getMetadata().get("http-method")),
                result.getProvider().getAddress(),
                result.getProvider().getPort(),
                result.getServiceUri(),
                result.getInterfaces().get(0).getInterfaceName(),
                null,
                matrix
        );

        // Ausgabe
        if (statusDTO.getMessage() == null)
            return new ResponseEntity<>(statusDTO, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Matrixfarbe beim Provider aendern (POST Anfrage)
     *
     * @return
     */
    @PostMapping(path = "/light")
    public ResponseEntity setLight() {
        // Fehlerausgabe, falls Dienst nicht gefunden werden kann
        if (!receiveOrchestrationData("set-information-matrix-light", "HTTP-INSECURE-JSON", HttpMethod.POST))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        // Matrixfarbe beim Provider setzen
        MatrixDTO matrix = new MatrixDTO(true, MatrixColorMapper.singleColor(Color.RED));
        OrchestrationResultDTO result = this.data.get("set-information-matrix-light");
        StatusDTO statusDTO = arrowheadService.consumeServiceHTTP(
                StatusDTO.class,
                HttpMethod.valueOf(result.getMetadata().get("http-method")),
                result.getProvider().getAddress(),
                result.getProvider().getPort(),
                result.getServiceUri(),
                result.getInterfaces().get(0).getInterfaceName(),
                null,
                matrix
        );

        // Ausgabe
        if (statusDTO.getMessage() == null)
            return new ResponseEntity<>(statusDTO, HttpStatus.OK);
        else
            return new ResponseEntity<>(statusDTO.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
