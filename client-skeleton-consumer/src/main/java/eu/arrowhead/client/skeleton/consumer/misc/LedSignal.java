package eu.arrowhead.client.skeleton.consumer.misc;

import com.pi4j.io.gpio.*;

/**
 * LED-Steuerung
 * Blinken der LED
 */
public class LedSignal extends Thread {
    private GpioPinDigitalOutput gpioPinDigitalOutput;
    private GpioController gpio;
    private boolean stop = false;
    private long sleepTime;

    /**
     * LED Informationen mitteilen
     *
     * @param pin
     * @param sleepTime
     */
    public LedSignal(Pin pin, long sleepTime) {
        this.gpio = GpioFactory.getInstance();
        this.gpioPinDigitalOutput = this.gpio.provisionDigitalOutputPin(pin, "LED", PinState.LOW);
        this.sleepTime = sleepTime;
    }

    /**
     * Status des Threads zurueckgeben
     *
     * @return
     */
    public boolean isStop() {
        return stop;
    }

    /**
     * Thread beenden
     *
     * @param stop
     */
    public void setStop(boolean stop) {
        this.stop = stop;
    }

    /**
     * LED in kontinuierlichem Rythmus blinken lassen
     */
    @Override
    public void run() {
        try {
            while (!this.isStop()) {
                gpioPinDigitalOutput.high();
                Thread.sleep(sleepTime);
                gpioPinDigitalOutput.low();
                Thread.sleep(sleepTime);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            gpio.shutdown();
        }

        // Aufraeumen
        if (this.isStop()) {
            gpioPinDigitalOutput.low();
            gpio.shutdown();
        }
    }
}
