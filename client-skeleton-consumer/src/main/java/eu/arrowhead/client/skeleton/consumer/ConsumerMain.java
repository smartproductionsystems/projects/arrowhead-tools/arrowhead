package eu.arrowhead.client.skeleton.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import eu.arrowhead.common.CommonConstants;

/**
 * Startklasse
 */
@SpringBootApplication
@ComponentScan(basePackages = {CommonConstants.BASE_PACKAGE}) //TODO: add custom packages if any
public class ConsumerMain {
    public static void main( final String[] args ) {
    	SpringApplication.run(ConsumerMain.class, args);
    }
}
